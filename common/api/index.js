import request from '@/common/request/ajaxRequest'
// 登录认证
export const authenticate = (userName,password) => {
  return request({
    url: '/business/user/authenticate',
    method: 'POST',
    data:{
			userName:userName,
			password:password
		},
		requireAuth:false,//检查是否需要登录
  })
}
// 获取首页的系统列表
export const getSystemList = (current,pageSize) => {
  return request({
    url: '/business/common/getSystemList',
    method: 'GET',
    params:{
			current:current,
			pageSize:pageSize
		},
		requireAuth:true,//检查是否需要登录
  })
}
// 获取风场列表
export const getWindfieldList = (current,pageSize) => {
  return request({
    url: '/business/common/getProjectTestByUser',
    method: 'GET',
    params:{
			current:current,
			pageSize:pageSize
		},
		requireAuth:true,//检查是否需要登录
  })
}
// 获取风机列表
export const getFanList = (current,pageSize,pjId) => {
  return request({
    url: '/business/common/getUpByPjId',
    method: 'GET',
    params:{
			current:current,
			pageSize:pageSize,
			pjId:pjId
		},
		requireAuth:true,//检查是否需要登录
  })
}
// 根据机组id获取施工阶段数据
export const getordersByUpId = (upId,current,pageSize) => {
  return request({
    url:'/business/common/getordersByUpId',
    method: 'get',
		dataType:'json',
    params: {
			upId:upId,
			current:current,
			pageSize:pageSize
		}
  })
}
// 根据机组id获取螺栓配置列表
export const getConfigurationByUpId = (upId,current,pageSize) => {
  return request({
    url: '/business/common/getConfigurationByUpId',
    method: 'get',
		dataType:'json',
    params: {
			upId:upId,
			current:current,
			pageSize:pageSize
		}
  })
}
// 获取审核列表
export const getApplyList = (status,current,pageSize,applyType) => {
  return request({
    url:'/business/common/getApplyList',
    method: 'get',
		dataType:'json',
    params: {
			status:status,
			current:current,
			pageSize:pageSize,
			applyType:applyType
		}
  })
}
// 审核操作
export const examine = (status,id,remarks,type) => {
  return request({
    url:'/business/common/examine',
    method: 'post',
		dataType:'json',
    data: {
			status:status,
			id:id,
			remarks:remarks,
			type:type
		}
  })
}
// 获取用户信息
export const getUserInfo = () => {
  return request({
    url:'/business/user/getUserInfo',
    method: 'get',
		dataType:'json',
    params: {}
  })
}
// 获取消息列表
export const getSendList = (current,pageSize) => {
  return request({
    url:'/business/common/getSendList',
    method: 'get',
		dataType:'json',
    params: {
			current:current,
			pageSize:pageSize
		}
  })
}
// 读消息
export const read = (id) => {
  return request({
    url:'/business/common/read',
    method: 'get',
		dataType:'json',
    params: {
			id:id
		}
  })
}
// 获取文件上传列表
export const getBoltList = (status,current,pageSize) => {
  return request({
    url: '/business/common/getBoltList',
    method: 'get',
		dataType:'json',
    params: {
			status:status,
			current:current,
			pageSize:pageSize
		}
  })
}
// 获取审核，申请操作历史
export const getHistory = (type,upId) => {
  return request({
    url: '/business/common/operateLog',
    method: 'get',
		dataType:'json',
    params: {
			type:type,
			upId:upId
		}
  })
}
// 历史对比
export const historicalComparison = (upId) => {
  return request({
    url: '/business/common/getCompareList',
    method: 'get',
		dataType:'json',
    params: {
			upId:upId
		}
  })
}

