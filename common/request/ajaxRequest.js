import uniRequest from 'uni-request';
import {baseURL} from '@/common/utils/util.js'
uniRequest.defaults.baseURL = baseURL;
// uniRequest.defaults.headers.common['Authorization'] = AUTH_TOKEN;
uniRequest.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
uniRequest.defaults.headers.accessToken = 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIyOTY4MiIsImlhdCI6MTYwNjg4NzU5OCwiYXV0aG9yaXRpZXMiOiJ7XCJhcHBseVR5cGVcIjowLFwidXNlck5hbWVcIjpcIuaymeWFi-e7tFwiLFwidXNlcklkXCI6XCIyOTY4MlwifSIsImV4cCI6MTYwNjk3Mzk5OH0.Sb23CBle3960nTO7F_sm_IovSl4qo_e6WKTUYZK1way8o9FTM6QXZxo3QJZvTKTaJEa9OWXeQr8QhYjkU1ir_w';
//HTTPrequest拦截
uniRequest.interceptors.request.use(config => {
	uni.showLoading({
		title: 'loading',
		mask:true
	});
	// 检查该接口是否需要登录
	if(config.requireAuth){
		try {
			const value = uni.getStorageSync('storage_key');
			if (value) {
				if(value){
					console.log('Request monitoring：Logged in')
					return config
				}
			}else{
				uni.hideLoading()
				console.log('Request monitoring：Not logged in')
				uni.reLaunch({
				  url: '/pages/public/login'
				});
			}
		} catch (e) {
		  console.log(e)
		}
	}else{
		return config
	}
	
}, error => {
  return Promise.reject(error)
});
//HTTPresponse拦截
uniRequest.interceptors.response.use(res => {
  const status = res.data.code || '10000' || 'B1003';//自定义返回状态字段,如果不是code请更换
  const message = res.data.msg || '未知错误';
	uni.hideLoading()
  if (status !== '10000') {
    return Promise.reject(new Error(message))
  }else{
		return res;
	}
}, error => {
  return Promise.reject(new Error(error));
});
export default uniRequest